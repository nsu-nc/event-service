package ru.nsu.ccfit.nsunc.event

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
class EventApplication

fun main(args: Array<String>) {
	runApplication<EventApplication>(*args)
}