package ru.nsu.ccfit.nsunc.event.entities

import org.hibernate.annotations.GenericGenerator
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType

@Entity
@EnableAutoConfiguration
@Table(name= "subscriber")
data class Subscriber(
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriber_seq")
        @GenericGenerator(name = "subscriber_seq", strategy = "increment")
        var id: Int? = null,

        @Column(name = "mail")
        var mail : String? = null
)
