package ru.nsu.ccfit.nsunc.event.entities

import org.hibernate.annotations.GenericGenerator
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn


@Entity
@EnableAutoConfiguration
@Table(name = "event_subscribers")
data class EventsSubscribers(
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sub_seq")
        @GenericGenerator(name = "sub_seq", strategy = "increment")
        var id: Int? = null,


        @ManyToOne
        @JoinColumn(name = "event_id", referencedColumnName = "id")
        var event: Event = Event(),

        @ManyToOne
        @JoinColumn(name = "sub_id", referencedColumnName = "id")
        var subscribers: Subscriber = Subscriber()
)
