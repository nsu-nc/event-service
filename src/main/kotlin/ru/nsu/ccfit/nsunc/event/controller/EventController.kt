package ru.nsu.ccfit.nsunc.event.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestBody
import ru.nsu.ccfit.nsunc.event.entities.Event
import ru.nsu.ccfit.nsunc.event.service.EventService


@RestController
@RequestMapping("event/")
class EventController {

    @Autowired
    private lateinit var eventService : EventService
    @PostMapping(value = ["create"])
    fun createEvent(@RequestParam eventName: String, @RequestParam eventType: String,
                    @RequestParam description: String): HttpStatus {
        return eventService.saveNewEvent(eventName, eventType, description)
    }

    @PostMapping(value = ["find"])
    fun findEvent(@RequestParam eventId: Int): HttpStatus {
        return eventService.findEvent(eventId)
    }

    @PostMapping(value = ["delete"])
    fun deleteEvent(@RequestParam eventId: Int): HttpStatus {
        return eventService.deleteEvent(eventId)
    }

    @PostMapping(value = ["update"])
    fun updateEvent(@RequestBody event: Event): HttpStatus {
      return eventService.saveUpdateEvent(event)
    }
}