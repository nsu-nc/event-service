package ru.nsu.ccfit.nsunc.event.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import ru.nsu.ccfit.nsunc.event.entities.Event
import ru.nsu.ccfit.nsunc.event.entities.History
import ru.nsu.ccfit.nsunc.event.repository.EventRepository
import ru.nsu.ccfit.nsunc.event.repository.EventSubscriberRepository
import ru.nsu.ccfit.nsunc.event.repository.HistoryRepository


@Service
class EventServiceImpl : EventService {

    @Autowired
    private lateinit var eventRepository: EventRepository

    @Autowired
    private lateinit var historyRepository: HistoryRepository

    @Autowired
    private lateinit var eventSubscriberRepository: EventSubscriberRepository

    override fun saveNewEvent(eventName: String, eventType: String, description: String): HttpStatus {
        eventRepository.findAll().forEach {
            if (it.name == eventName) {
                return HttpStatus.BAD_REQUEST
            }
        }
        val newEvent = Event(name = eventName)
        eventRepository.save(newEvent)
        eventRepository.findById(newEvent.id).orElse(null)?.let {
            History(event = it, eventType = eventType, description = description)
        }?.let {
            historyRepository.save(it)
        }
        return HttpStatus.CREATED
    }

    override fun deleteEvent(eventId: Int): HttpStatus {
        if (eventRepository.findById(eventId).orElse(null) == null) {
            return HttpStatus.BAD_REQUEST
        }

        historyRepository.findAll().forEach {
            if (it.event.id == eventId) {
                historyRepository.delete(it)
            }
        }
        eventSubscriberRepository.findAll().forEach {
            if (it.event.id == eventId) {
                eventSubscriberRepository.delete(it)
            }
        }
        eventRepository.deleteById(eventId)
        return HttpStatus.OK
    }

    override fun findEvent(eventId: Int): HttpStatus {
        if (eventRepository.findById(eventId).orElse(null) != null) {
            return HttpStatus.OK
        }
        return HttpStatus.BAD_REQUEST
    }

    override fun saveUpdateEvent(event: Event): HttpStatus {
        val updateEvent = eventRepository.findById(event.id).orElse(null)
        if (updateEvent != null) {
            updateEvent.name = event.name
            updateEvent.deleted = event.deleted
            updateEvent.subAbility = event.subAbility
            eventRepository.save(updateEvent)
            return HttpStatus.OK
        }
        return HttpStatus.BAD_REQUEST
    }
}