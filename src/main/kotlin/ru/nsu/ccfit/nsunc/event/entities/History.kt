package ru.nsu.ccfit.nsunc.event.entities

import org.hibernate.annotations.GenericGenerator
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn


@Entity
@EnableAutoConfiguration
@Table(name = "history")
data class History(
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "his_seq")
        @GenericGenerator(name = "his_seq", strategy = "increment")
        var id: Int? = null,

        @ManyToOne
        @JoinColumn(name = "event_id", referencedColumnName = "id")
        var event: Event = Event(),

        @Column(name = "event_type")
        var eventType: String = "",

        @Column(name = "description")
        var description: String = ""
)
