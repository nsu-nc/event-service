package ru.nsu.ccfit.nsunc.event.entities

import org.hibernate.annotations.GenericGenerator
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType


@Entity
@EnableAutoConfiguration
@Table(name = "event")
data class Event(

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_seq")
        @GenericGenerator(name = "pet_seq", strategy = "increment")
        var id: Int = 0,

        @Column(name = "name")
        var name: String? = null,

        @Column(name = "sub_ability")
        var subAbility: Boolean = false,

        @Column(name = "deleted")
        var deleted: Boolean = false
)
