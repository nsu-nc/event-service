package ru.nsu.ccfit.nsunc.event.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.nsu.ccfit.nsunc.event.entities.History

@Repository
interface HistoryRepository : CrudRepository<History, Int> {}