package ru.nsu.ccfit.nsunc.event.service

import org.springframework.http.HttpStatus
import ru.nsu.ccfit.nsunc.event.entities.Event

interface EventService {
    fun saveNewEvent(eventName: String, eventType: String, description: String): HttpStatus

    fun saveUpdateEvent(event : Event): HttpStatus

    fun findEvent(eventId : Int): HttpStatus

    fun deleteEvent(eventId : Int): HttpStatus
}